import argparse
from socket import gethostbyname, gethostbyaddr

from tracer.tracer import traceroute, MapGenerator

parser = argparse.ArgumentParser(description="Traceroute in Python")
parser.add_argument("hostname", type=str, help="The hostname of the Destination")
parser.add_argument("hops", type=int, default=30, help="Hop count")
parser.add_argument("--map", help="Generate a Map", action="store_const", const=map)


def reverse_lookup(ip_addr: str) -> str:
    """
    Does a reverse lookup for the hostname
    :param ip_addr:  The ip addr as string
    :return: Either the hostname or a * if hostname could not be found
    """
    host = "*"
    try:
        host = gethostbyaddr(ip_addr)[0]
    except:
        pass

    return host


def execute(hostname: str, hops: int, generate_map: bool):
    ttl = 1
    ip = gethostbyname(hostname)
    port = 33434
    recv_addr = None
    visited = []
    sum: int = 0
    print(f"Tracing for Route {ip}")
    print(f"{'TTL':<4} {'Hostname': <40} {'IP Address': <15} {'Response Time'}")
    while hops > ttl and ip != recv_addr:
        res = traceroute(hostname, port, ttl)
        recv_addr = res.ip_addr
        if ttl is not 1 and ip != recv_addr and recv_addr != "*" and recv_addr not in visited:
            visited.append(recv_addr)

        host = reverse_lookup(recv_addr)
        print(f"({ttl: <2}) {host: <40} {res.ip_addr: <15} {res.time:.{0}f}ms")
        ttl = ttl + 1
        sum = sum + res.time

    if (generate_map):
        print("Generating Map")
        MapGenerator(visited, "map.html")

    print(f"Trace finished in {sum:.{0}f}ms")


def main():
    args = parser.parse_args()
    hostname = args.hostname
    hops = args.hops
    generate_map = args.map is not None
    execute(hostname, hops, generate_map)


if __name__ == "__main__":
    main()
