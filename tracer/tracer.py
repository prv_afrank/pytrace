import socket
import time
from collections import namedtuple
from typing import List

import folium
from ip2geotools.databases.noncommercial import DbIpCity
from ip2geotools.models import IpLocation

Point = namedtuple('Point', ['longitude', 'latitude', 'cityname'])
Trace = namedtuple("Trace", ["ip_addr", "time"])


def traceroute(hostname: str, port: int, ttl: int) -> Trace:
    dest_ip = socket.gethostbyname(hostname)

    sender = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM, proto=socket.IPPROTO_UDP)
    sender.setsockopt(socket.SOL_IP, socket.IP_TTL, ttl)

    receiver = socket.socket(family=socket.AF_INET, type=socket.SOCK_RAW, proto=socket.IPPROTO_ICMP)
    receiver.bind(('', port))

    sender.sendto(b'', (dest_ip, port))

    try:
        receiver.settimeout(15)
        start = time.time()
        data, addr = receiver.recvfrom(1024)
        trace: Trace = Trace(ip_addr=addr[0], time=((time.time() - start) * 1000))
        return trace
    except socket.timeout as e:
        trace: Trace = Trace(ip_addr='*', time=((time.time() - start) * 1000))
        return trace
    finally:
        sender.close()
        receiver.close()


def locate(addr: str) -> Point or None:
    try:
        result: IpLocation = DbIpCity.get(addr, api_key='free')
        return Point(longitude=result.longitude, latitude=result.latitude, cityname=result.city)
    except:
        return None


class MapGenerator:
    def __init__(self, addrs: List[str], filename):
        if addrs is None or not len(addrs):
            return

        locations: List[Point] = [x for x in (locate(y) for y in addrs) if x is not None]

        point = locations[0]
        map = folium.Map(location=[point.latitude, point.longitude])

        folium.Marker(location=[point.latitude, point.longitude], icon=folium.Icon(color="red"),
                      tooltip=f"{point.cityname}\nLatitude:{point.latitude}\nLongitude:{point.longitude}").add_to(map)

        if len(locations) is 1:
            return

        for x in locations[1:len(locations) - 1]:
            folium.Marker(location=[x.latitude, x.longitude],
                          tooltip=f"{x.cityname}\nLatitude:{x.latitude}\nLongitude:{x.longitude}").add_to(
                map)
            folium.PolyLine(locations=[[point.latitude, point.longitude], [x.latitude, x.longitude]]).add_to(map)
            point = x

        x = point
        point = locations[-1]

        folium.Marker(location=[point.latitude, point.longitude], icon=folium.Icon(color="green"),
                      tooltip=f"{point.cityname}\nLatitude:{point.latitude}\nLongitude:{point.longitude}").add_to(map)
        folium.PolyLine(locations=[[x.latitude, x.longitude], [point.latitude, point.longitude]]).add_to(map)

        map.save(filename)
