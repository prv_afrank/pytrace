# PyTrace
A simple terminal based application which does `traceroute`.
You have to enter the hostname and how many hops should be made.
This is merely a project for fun, so don't expect it to be working like the "real" implementation of `traceroute` under Linux or Windows.

Because this application access raw socket, it must be started as `root` via `sudo`

```bash
sudo ./env/bin/python3 . google.com 30
```

The end result is a generated map called `map.html` via `folium`, which represents the route from the first hop (excluding your local router).
The red marker is the start point, the green marker is the end point. The map is generate via a a webcall to a service which resolves the IP Address to a Geolocation similar to GeoIP.

Requirements can be installed via

```bash
pip3 install -r requirements.txt
```

The output format looks something like this
```bash
TTL  Hostname                                 IP Address         Response Time
(1 ) localhost                                127.0.0.1          3ms
Trace finished in 3ms

```